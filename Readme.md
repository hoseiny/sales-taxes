# Overview

The current project is a very simple Ruby application to calculate and prints out the receipt details for a simple shopping baskets. It accepts a list of
products, then prints a receipt for those products by calculating the taxes paid based on two types of applicable tax types, i.e. imported goods and non-exemption sale tax goods.

# Packaging

The ruby verison used is ruby 1.9.3p545
Rspec version used is version > 3.0
to install gems required please run `bundle install`

# Usage

Provide an exemptions yaml file that lists of goods which are exempt from sales tax. The file name can be simply 'exemptions.yml'.
If a product matches an exemption good, it will be excluded from sales tax.

Put the input file into another text file and pass it to the main executable application file as:
    main_app.rb input.txt


# Running specs

Please run all specs by:
       `rspec spec/*_spec.rb`


