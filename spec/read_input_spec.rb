require 'spec_helper'

module SalesTaxes
  describe ReadInput do
    let(:reader) { SalesTaxes::ReadInput }


    context "read one item line" do
      let(:input_line) { "1, book reza, 12.49" }
      before(:each) do
        @line_item = reader.to_line_item(input_line)
      end

      it "should read descriptions correctly, then produce a non empty new line item" do
        @line_item.should_not be_empty
      end

      it "should read product quantity correctly" do
        @line_item[:quantity].should eq("1")
      end

      it "should read product price correctly" do
        @line_item[:price].should eq("12.49")
      end

      it "should read product name correctly" do
        @line_item[:name].should eq("book reza")
      end
    end

    context "read an input file with header" do
      let(:input_dir) { "spec/input/"}
      let(:input_file) { input_dir + "input_file1.txt" }
      before(:each) do
        @line_items = reader.read_file(input_file)
      end
      it "should read the input file correctly" do
        @line_items.length.should == 3
      end
    end
  end
end