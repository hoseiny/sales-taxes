require 'spec_helper'

module SalesTaxes
  describe LineItem do
    describe :initialize do
      let(:line_item_instance) { LineItem.new(a_line_item) }
      let(:a_line_item) { {:quantity => a_quantity, :name => a_name, :price => a_price} }
      let(:a_name)      { "reza book" }
      let(:a_price)     { 9.48 }
      let(:a_quantity)  { 4 }

      it "should set the product correctly" do
        line_item_instance.product.name.should == a_name
        line_item_instance.product.price.should == a_price
      end

      it "should set the quantity correctly" do
        line_item_instance.quantity.should == a_quantity
      end
    end
  end
end