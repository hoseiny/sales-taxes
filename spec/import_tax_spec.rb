require 'spec_helper'

module SalesTaxes
  describe ImportTax do
    let(:imported_tax) { ImportTax }
    let(:non_imported_product) { Product.new("non- import beer", price) }
    let(:imported_product) { Product.new("imported medical", price) }
    let(:price) { 9.50 }

    describe :applies_to? do
      it "should not apply to a non-imported product" do
        imported_tax.applies_to?(non_imported_product).should == false
      end

      it "should not apply to an imported product" do
        imported_tax.applies_to?(imported_product).should == true
      end
    end

    describe :price_after_tax do
      context "an imported product" do
        it "calculate the imported tax duty after applying to the original price" do
          imported_tax.tax_of(imported_product).should == (price * imported_tax.imported_tax_duty).to_f.round(2)
        end
      end

      context "a non-imported product" do
        it "calculate the imported tax duty after applying to the original price" do
          imported_tax.tax_of(non_imported_product).should == 0
        end
      end
    end

  end
end