require 'spec_helper'

module SalesTaxes
  describe Outputer do
    let(:outputter)   { Outputer.new(receipt_instance) }
    let(:input_dir)   { "spec/input/" }
    let(:output_dir)  { "spec/output/" }
    let(:input_file)  { input_dir + "input_file2.txt" }
    let(:output_file) { output_dir + "output_file2.txt" }
    let(:yaml_file)   { input_dir + "exemptions.yml" }
    let(:expected_output)   { File.read(output_file) }
    let(:receipt_instance)  { Receipt.new(SalesTaxes::ReadInput.read_file(input_file)) }

    before(:each) do
      @formatted_output = outputter.print
    end

    it "should create correct formatted output" do
      @formatted_output.should == expected_output
    end
  end
end
