require 'spec_helper'

module SalesTaxes
  describe Receipt do

    let(:input_dir)   { "spec/input/" }
    let(:input_file)  { input_dir + "input_file2.txt" }
    let(:yaml_file)   { input_dir + "exemptions.yml" }

    before(:each) do
      @receipt_instance = Receipt.new(SalesTaxes::ReadInput.read_file(input_file))
    end

    describe :total_taxes do
      it "should calculate correct total taxes" do
        @receipt_instance.total_taxes.should == 7.79
      end
    end

    describe :total_amount do
      it "should calculate correct total amount including taxes" do
        @receipt_instance.total_amount.should ==  75.59
      end
    end
  end
end

