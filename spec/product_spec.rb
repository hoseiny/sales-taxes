require 'spec_helper'

module SalesTaxes
  describe Product do

    describe :price do
      let(:product_instance) { Product.new(_name, price) }
      let(:_name) { "reza book" }
      context "with two digit price" do
        let(:price) { 9.48 }
        it "should set the price" do
          product_instance.price.should == 9.48
        end
      end

      context "with three digit price" do
        let(:price) { 9.487 }
        it "should round the price " do
          product_instance.price.should == 9.49
        end
      end
    end

    describe :name do
      let(:product_instance) { Product.new(name, _price) }
      let(:name) { "reza book" }
      let(:_price) { 9.487 }
      it "should set the name" do
        product_instance.name.should == name
      end
    end

    describe :price_after_applicable_taxes do
      let(:product_instance) { Product.new(name, price) }
      let(:price) { 10 }

      context "non_imported free exemption sale tax product" do
        let(:name) { "reza book" }
        it "should set the price after tax correctly" do
          product_instance.price_after_applicable_taxes.should == 10
        end
      end

      context "imported but exemption sale tax product" do
        let(:name) { "reza imported book" }
        it "should set the price after tax correctly" do
          product_instance.price_after_applicable_taxes.should == 10.5
        end
      end

      context "non imported with a sale tax product" do
        let(:name) { "reza beer" }
        it "should set the price after tax correctly" do
          product_instance.price_after_applicable_taxes.should == 11
        end
      end

      context "both imported and a sale tax applicable product" do
        let(:name) { "reza imported beer" }
        it "should set the price after tax correctly" do
          product_instance.price_after_applicable_taxes.should == 11.5
        end
      end


    end
  end
end