require 'spec_helper'

module SalesTaxes
  describe SalesTax do
    describe :exemptions do
      let(:input_dir)             { "spec/input/" }
      let(:yaml_file)             { input_dir + "exemptions.yml" }
      let(:sales_tax)             { SalesTax }
      let(:non_exempt_product)    { Product.new("beer", price) }
      let(:exempt_product)        { Product.new("medical", price) }
      let(:price)                 { 9.50 }


      before(:each) do
        sales_tax.exemptions_source_file(yaml_file)
      end

      it "should read YAML source file" do
        sales_tax.exemptions.should == ["book", "food", "medical", "pill"]
      end

      describe :applies_to? do
        it "should not apply to a non-exempt product" do
          sales_tax.applies_to?(non_exempt_product).should == true
        end

        it "should apply to a real exempt product" do
          sales_tax.applies_to?(exempt_product).should == false
        end
      end

      describe :price_after_tax do
        context "an exempt product" do
          it "calculate the tax after applying to the original price" do
            sales_tax.tax_of(exempt_product).should == 0
          end
        end

        context "a non-exempt product" do
          it "calculate the tax after applying to the original price" do
            sales_tax.tax_of(non_exempt_product).should == (price * sales_tax.sales_tax_rate).to_f.round(2)
          end
        end
      end
    end
  end
end
