require 'bigdecimal'

module SalesTaxes
  class Product
    attr_accessor :price, :name, :price_after_tax

    def initialize(name, price)
      @price = price.to_f.round(2)
      @name = name
    end

    def price_after_applicable_taxes
      price_after_tax = BigDecimal(@price, 2) + tax_amount
    end

    def tax_amount
      total_tax_amount = BigDecimal(0, 2)
      applicable_taxes.map{ |tax|
         if tax.applies_to?(self)
          total_tax_amount += tax.tax_of(self)
        end
      }
      total_tax_amount
    end

    def applicable_taxes
      [SalesTaxes::ImportTax ,SalesTaxes::SalesTax ]
    end
  end
end

