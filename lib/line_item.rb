module SalesTaxes
  class LineItem
    attr_accessor :product, :quantity

    def initialize(line_item)
      unless line_item.empty?
        self.product = SalesTaxes::Product.new(line_item[:name], line_item[:price])
        self.quantity = line_item[:quantity]
      end
    end

    def tax_amount
      BigDecimal.new(self.product.tax_amount * self.quantity.to_i, 2)
    end

    def price_after_tax
      BigDecimal.new(self.product.price_after_applicable_taxes * self.quantity.to_i, 2)
    end
  end
end
