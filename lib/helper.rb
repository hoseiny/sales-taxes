module SalesTaxes
  class Helper
    def self.load_from(input_file)
      receipt = Receipt.new(SalesTaxes::ReadInput.read_file(input_file))
      Outputer.new(receipt).print
    end
  end
end