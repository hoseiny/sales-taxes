require 'yaml'

module SalesTaxes
  class SalesTax
    attr_accessor :exemptions, :exemptions_source_file

    def self.exemptions
      yaml_load = YAML.load(File.open(exemptions_source_file))
      @exemptions ||= yaml_load["exemptions"]
    end

    def self.exemptions_source_file(input=nil)
      input || 'exemptions.yml'
    end

    def self.sales_tax_rate
      0.1
    end

    def self.applies_to?(product)
      !exempt_from_tax(product.name)
    end

    def self.tax_of(product)
      if applies_to?(product) then
        (product.price * sales_tax_rate).to_f.round(2)
      else
        0
      end
    end

    private
    def self.exempt_from_tax(product_name)
      exemptions.each do |exemption|
        return true if product_name =~ /#{exemption}/
      end
      false
    end
  end
end


