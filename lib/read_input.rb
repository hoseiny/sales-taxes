require 'colored'

module SalesTaxes
  class ReadInput

    def self.read_file(file_name)
      line_items = Array.new
      f = File.open(file_name, "r")
      begin
        f.each_line do |line|
          item = to_line_item(line)
          line_items.push(item) unless item.nil?
        end if File.exists?(file_name)
        line_items
      rescue Exception => detail
        STDERR.puts "Processing of #{file_name} failed caused by #{detail}"
      ensure
        f.close unless f.nil?
      end

    end

    def self.to_line_item(input_line)
      line_item_match_data = entry_format.match(input_line).to_a # An array of Match groups type showing [quantity, product name , and product price]
      unless line_item_match_data.empty?
        line_item = Hash.new
        line_item[:quantity] = line_item_match_data[1]
        line_item[:name] = line_item_match_data[2]
        line_item[:price] = line_item_match_data[3]
        line_item
      end
    end

    def self.entry_format
      regex = /(\d+),\s+([\w\s]+),\s+(\d+.\d+)$/
    end
  end
end