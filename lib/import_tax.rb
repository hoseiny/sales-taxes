module SalesTaxes
  class ImportTax
    def self.applies_to?(product)
      product.name =~ /imported/ ? true : false
    end

    def self.imported_tax_duty
      0.05
    end

    def self.tax_of(product)
      if applies_to?(product) then
        return (product.price * imported_tax_duty).to_f.round(2)
      else
        return 0
      end
    end
  end
end
