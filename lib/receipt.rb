module SalesTaxes
  class Receipt
    attr_accessor :line_items

    def initialize(hashed_line_items)
      @line_items = Array.new
      hashed_line_items.each { |hashed_line_item|
        @line_items.push(LineItem.new(hashed_line_item))
      }
    end

    def total_taxes
      sum = 0
      @line_items.map { |line_item|
        sum += line_item.tax_amount
      }
      sum.to_f.round(2)
    end

    def total_amount
      sum = 0
      @line_items.map { |line_item|
        sum += line_item.price_after_tax
      }
      sum.to_f.round(2)
    end
  end
end
