module SalesTaxes
  class Outputer
    attr_accessor :receipt

    def initialize(receipt)
      @receipt = receipt
    end

    def sales_tax_line
      "Sales Taxes: #{receipt.total_taxes}"
    end

    def total_line
      "Total: #{receipt.total_amount}"
    end

    def formatted_line_item(line_item)
      "#{line_item.quantity}, #{line_item.product.name}, #{line_item.product.price_after_applicable_taxes.to_f.round(2)}"
    end

    def print
      formatted_output = String.new
      @receipt.line_items.each { |line_item|
        formatted_output += formatted_line_item(line_item) + "\n"
      }
      formatted_output += "\n" + sales_tax_line + "\n"
      formatted_output += total_line
      formatted_output
    end
  end
end