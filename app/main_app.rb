module SalesTaxes
  full_lib_path = File.expand_path(File.dirname(__FILE__) + "/../lib/")
  full_spec_path = File.expand_path(File.dirname(__FILE__) + "/../spec/")
  Dir.glob(full_lib_path + "/*.rb").each do |file|
    require file
  end

  begin
    input_file = ARGF.filename != "-" ? ARGF.filename : full_spec_path + '/input/input_file2.txt'
    output = Helper.load_from(input_file)
    $stdout.puts output
  rescue Exception => details
    $stderr.puts "can't load #{input_file} \t due to: #{details}"
  end

end